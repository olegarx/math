#include "Math.h"
#include <cmath>

Math::Math() {
}

double Math::function(double a, double b) {
	double sinResult1 = std::sin(a);
	double sinResult2 = std::sin(sinResult1);
	double sinResult3 = std::sin(sinResult2);
	return (sinResult3 + b);
}