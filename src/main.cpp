#include "math/Math.h"
#include <iostream>

int main() {
	double a = 10;
	double b = 10;

	Math math;

	std::cout << "sin(sin(sin(" << a << "))) + " << b << " = " << math.function(a, b) << std::endl;

	return 0;
}